// 使用localstorage时，在外面最好加一个try catch，解决用户关闭浏览器本地存储时功能或使用隐身模式时浏览器抛出异常
export default {
		changeCity (state, city) {
			state.city = city
			try {
				localStorage.city = city
			} catch (e) {}
		}
	}