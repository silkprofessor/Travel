// 使用localstorage时，在外面最好加一个try catch，解决用户关闭浏览器本地存储时功能或使用隐身模式时浏览器抛出异常
let defaultCity = '郑州'
try {
	if (localStorage.city) {
		defaultCity = localStorage.city
	}
} catch (e) {}

export default {
		// city默认值优先从localstorage（本地存储）取，取不到则采用设置好的默认值
		city: defaultCity
	}