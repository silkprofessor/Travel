import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'
// vuex是一个插件，vue中使用插件如下
Vue.use(Vuex)

// 导出vuex创建的一个仓库,仓库中有一个state存放全局共用数据
export default new Vuex.Store({
	state,
	// action调用mutations借助commit方法，在此可借助ctx拿到commit方法
	// actions: {
	// 	changeCity (ctx, city) {
	// 		ctx.commit('changeCity', city)
	// 	}
	// },
	mutations
})